/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact;

import java.awt.EventQueue;
import richtrmoc.space_impact.presenter.JFrame;

/**
 *
 * @author lukas.richtrmoc
 */
public class main {

	public static JFrame jf;
	
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				jf = new JFrame();
				jf.addBoard();
			}
		});
	}
	
}
