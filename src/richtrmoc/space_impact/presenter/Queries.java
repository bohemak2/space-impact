/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.presenter;

import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.Hashtable;
import richtrmoc.space_impact.interfaces.Query;

/**
 *
 * @author lukas.richtrmoc
 */
public class Queries {
	
	private Hashtable<String,Query> data;

	public Queries() {
		data = new Hashtable<>();
	}

	public void addQuery(String id, Query query){
		data.put(id, query);
	}
	
	public boolean isQuery(String id){
		return data.containsKey(id);
	}
	
	public Query getQuery(String id){
		return data.get(id);
	}
	
	public void removeQuery(String id){
		data.remove(id);
	}
	
	public boolean hasQueries(){
		return data.size() > 0;
	}
	
	public void keyReleased(KeyEvent e){
		Enumeration el = data.elements();
		
		while (el.hasMoreElements()){
			Query q = (Query)el.nextElement();
			if (!q.isDone() && !q.isPause()){
				q.keyReleased(e);
			}
		}
	}
	
}