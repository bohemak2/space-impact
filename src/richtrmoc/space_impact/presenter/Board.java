/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.presenter;

/**
 *
 * @author lukas.richtrmoc
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;
import richtrmoc.space_impact.main;
import richtrmoc.space_impact.model.Alien;
import richtrmoc.space_impact.model.Game;
import richtrmoc.space_impact.model.Score;
import richtrmoc.space_impact.model.ScoreList;
import richtrmoc.space_impact.model.Shot;


public class Board extends JPanel implements ActionListener {

	private Timer timer;
	private int timer_ten;
	private Queries queries;
	private ScoreList scorelist;
	private Menu menu;
	private Game game;
	private final int DELAY = 10;

	public Board() {
		addKeyListener(new TAdapter());
		setFocusable(true);
		setBackground(Color.BLACK);
		
		timer_ten = 0;
		queries = new Queries();
		scorelist = new ScoreList();
		menu = new Menu();
		timer = new Timer(DELAY, this);
		timer.start();
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Queries getQueries() {
		return queries;
	}

	public void clearScorelist(int i) {
		scorelist.get().remove(i);
		scorelist.save();
	}

	public void changeScorelist() {
		scorelist.sort();
	}
	
	public void changeScorelist( boolean best){
		scorelist.sort(best);
	}
	
	public void editScorelist(int i){
		if (! queries.isQuery("SCORE_EDIT")){
			Input input= new Input();
			input.setMax(5);
			input.setMin(1);
			queries.addQuery("SCORE_EDIT", input);
		}
	}
	
	public void searchScorelist(){
		if (! queries.isQuery("SCORE_SEARCH")){
			Input input= new Input();
			input.setMax(5);
			input.setMin(1);
			queries.addQuery("SCORE_SEARCH", input);
		}
	}


	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if ( menu.isOpen()){
			drawMenu(g);
		}else{
			drawGame(g);
		}

		Toolkit.getDefaultToolkit().sync();
	}

	private void drawMenu(Graphics g){
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setFont(new Font("Sans", Font.PLAIN, 50));
		g2d.setColor(new Color(78,78,78));
		
		g2d.drawString("SPACE IMPACT", 20, 60);
		
		Font font = new Font("TimesRoman", Font.PLAIN, 20);
		g2d.setFont(font);
		g2d.fillRect(menu.getCenter(font) - 10, menu.getMiddle(font) - 50, getFontMetrics(font).stringWidth("NEW GAME") + 20 , menu.getItems().length * 30 + 5);
		
	
		for( int i = 0 ; i < menu.getItems().length ; i++){
			if ( menu.getPointer() == i ){
				g2d.setColor(new Color(133, 13, 13));
			}else{
				g2d.setColor(new Color(255, 255, 255));
			}
			g2d.drawString(menu.getItems()[i], menu.getCenter(font), menu.getMiddle(font) - 50 + ( ( (i + 1) * 30 ) - 20 ) + 15);
		}
		
		if (queries.isQuery("SCORE")){
			g2d.setColor(new Color(0,120,200));
			g2d.fillRect(menu.getCenter(font) - 40, menu.getMiddle(font) - (10 * 30 / 2), getFontMetrics(font).stringWidth("TOP 10 PLAYERS") + 20 , 10 * 35 + 5);
			
			int i = 0;
			g2d.setColor(new Color(255, 255, 255));
			g2d.drawString("TOP 10 PLAYERS", menu.getCenter(font) - 30, menu.getMiddle(font) - (12 * 30 / 2) + 55);
			
			int length = scorelist.get().size() < 10 ? scorelist.get().size() : 10;
			
			richtrmoc.space_impact.presenter.Score query = (richtrmoc.space_impact.presenter.Score)queries.getQuery("SCORE");
			
			for ( Score score : scorelist.get() ){
				if ( i < length && score.isShow()){
					if ( query.getPointer() >= length ){
						query.setPointer(0);
					}
					if ( query.getPointer() < 0){
						query.setPointer(length - 1);
					}
					if ( i == query.getPointer()){
						g2d.setColor(new Color(133, 13, 13));
					}else{
						g2d.setColor(new Color(255, 255, 255));
					}
					g2d.drawString(score.getName(), menu.getCenter(font) - 30,  (menu.getMiddle(font) - (10 * 30 / 2) + 70) + i * 30) ;
					g2d.drawString(Integer.toString(score.getResult()), menu.getCenter(font) + getFontMetrics(font).stringWidth("TOP 10 PLAYERS") / 2 + 20 ,(menu.getMiddle(font) - (10 * 30 / 2) + 70) + i * 30);
					i++;
				}
			}
			
			font = new Font("TimesRoman", Font.PLAIN, 9);
			g2d.setFont(font);
			g2d.setColor(new Color(78, 78, 78));
			
			g2d.drawString("close - (esc)ape", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("cancel - (esc)ape"),  main.jf.getContentPane().getHeight() - 10);
			
			if ( !scorelist.get().isEmpty()){
				g2d.drawString("search - (s)", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("cancel - (esc)ape"),  main.jf.getContentPane().getHeight() - 30);
				g2d.drawString("best/worst - (t)", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("cancel - (esc)ape"),  main.jf.getContentPane().getHeight() - 20);
				g2d.drawString("edit - (e)", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("cancel - (esc)ape"),  main.jf.getContentPane().getHeight() - 60);
				g2d.drawString("delete - (delete)", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("cancel - (esc)ape"),  main.jf.getContentPane().getHeight() - 50);
			}
			if ( queries.getQuery("SCORE").isDone()){
				queries.removeQuery("SCORE");
			}
		}
		
		if (queries.isQuery("SCORE_SEARCH")){
			richtrmoc.space_impact.presenter.Score query = (richtrmoc.space_impact.presenter.Score)queries.getQuery("SCORE");
			if (!query.isPause()){
				query.setPause(true);
			}
			
			Input input = (Input)queries.getQuery("SCORE_SEARCH");
			scorelist.search(input.getText());

			g2d.setColor(new Color(95,185,125));
			g2d.fillRect(menu.getCenter(font), menu.getMiddle(font) + (12 * 30 / 2) - 20, main.jf.getContentPane().getWidth() / 2 + 20 , (main.jf.getContentPane().getHeight() / 2 - (10 * 30 / 2)));
			
			g2d.setFont(new Font("TimesRoman", Font.PLAIN, 20));
			g2d.setColor(new Color(255, 255, 255));
			if ( input.getText().length() < input.getMax()){
				g2d.drawString("SEARCH: " + input.getText() + "_", menu.getCenter(font) + 20, menu.getMiddle(font) + (12 * 30 / 2) + 30);
			}else{
				g2d.drawString("SEARCH: " + input.getText(), menu.getCenter(font) + 20, menu.getMiddle(font) + (12 * 30 / 2) + 30);
			}
			
			font = new Font("TimesRoman", Font.PLAIN, 9);
			g2d.setFont(font);
			g2d.drawString("cancel - (esc)ape", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("cancel - (esc)ape"),  main.jf.getContentPane().getHeight() - 20);

			
			if ( input.done ){
				scorelist.search("");
				queries.removeQuery("SCORE_SEARCH");
				query.setPause(false);
			}
			
		}

		if (queries.isQuery("SCORE_EDIT")){
			richtrmoc.space_impact.presenter.Score query = (richtrmoc.space_impact.presenter.Score)queries.getQuery("SCORE");
			Score editing = scorelist.get().get(query.getPointer());
			Input input = (Input)queries.getQuery("SCORE_EDIT");
			
			if (!query.isPause()){
				query.setPause(true);
				input.setText(editing.getName());
			}
			
			g2d.setFont(new Font("TimesRoman", Font.PLAIN, 30));
			g2d.setColor(new Color(185,50,40));
			g2d.fillRect(menu.getCenter(font), menu.getMiddle(font), main.jf.getContentPane().getWidth() / 2 + 20 , main.jf.getContentPane().getHeight() / 2 + 10 );
			
			g2d.setColor(new Color(255, 255, 255));
			g2d.drawString("SCORE: " + Integer.toString(editing.getResult()), menu.getCenter(font) + 20, menu.getMiddle(font) + 50);
			
			if ( input.getText().length() < input.getMax()){
				g2d.drawString("NAME: " + input.getText() + "_", menu.getCenter(font) + 20, menu.getMiddle(font) + 100 );
			}else{
				g2d.drawString("NAME: " + input.getText(), menu.getCenter(font) + 20, menu.getMiddle(font) + 100 );
			}

			font = new Font("TimesRoman", Font.PLAIN, 9);
			g2d.setFont(font);
			g2d.setColor(new Color(0, 0, 0));
			g2d.drawString("min 1, max 5 chars", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("min 1, max 5 chars"),  main.jf.getContentPane().getHeight() - 40);
			g2d.setColor(new Color(255, 255, 255));
			g2d.drawString("cancel - (esc)ape", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("cancel - (esc)ape"),  main.jf.getContentPane().getHeight() - 20);
			g2d.drawString("save - (enter)", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("save - (enter)"),  main.jf.getContentPane().getHeight() - 10);

			if ( input.done ){
				if ( input.result ){
					editing.setName(input.getText());
					scorelist.save();
				}
				queries.removeQuery("SCORE_EDIT");
				query.setPause(false);
			}
		}
		
		if (game != null){
			g2d.setFont(new Font("TimesRoman", Font.PLAIN, 9));
			g2d.setColor(new Color(78, 78, 78));
			if (! queries.isQuery("SCORE")){
				g2d.drawString("(p)resume", main.jf.getContentPane().getWidth() - 50,  main.jf.getContentPane().getHeight() - 10);
			}
		}
	}
	
	private void drawGame(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		timer_ten += DELAY;
		
		g2d.setColor(new Color(133, 13, 13));
		g2d.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		
		
		if (game.getSpaceShip().getLife() == 0 || (game.getAliens().getLastAlien().getY() + game.getAliens().getLastAlien().getHeight()) >= game.getSpaceShip().getY()){
			Font font = new Font("TimesRoman", Font.PLAIN, 30);
			g2d.setFont(font);
			g2d.drawString("GAME OVER", (main.jf.getContentPane().getWidth() - getFontMetrics(font).stringWidth("GAME OVER")) / 2, (main.jf.getContentPane().getHeight() - 30) / 2 );
			g2d.setColor(new Color(255, 255, 255));
			g2d.drawString("YOUR SCORE: " + Integer.toString(game.getScore()), (main.jf.getContentPane().getWidth() - getFontMetrics(font).stringWidth("YOUR SCORE: " + Integer.toString(game.getScore()))) / 2, ( (main.jf.getContentPane().getHeight() - 30) / 2 ) + 30 );
			
			if (! queries.isQuery("NAME")){
				Input input= new Input();
				input.setMax(5);
				input.setMin(1);
				queries.addQuery("NAME", input);
			}
			
			Input input = (Input)queries.getQuery("NAME");
			if ( input.getText().length() < input.getMax()){
				g2d.drawString("NAME: " + input.getText() + "_", (main.jf.getContentPane().getWidth() - getFontMetrics(font).stringWidth("NAME: " + input.getText())) / 2, ( (main.jf.getContentPane().getHeight() - 30) / 2 ) + 100 );
			}else{
				g2d.drawString("NAME: " + input.getText(), (main.jf.getContentPane().getWidth() - getFontMetrics(font).stringWidth("NAME: " + input.getText())) / 2, ( (main.jf.getContentPane().getHeight() - 30) / 2 ) + 100 );
			}
			
			font = new Font("TimesRoman", Font.PLAIN, 9);
			g2d.setFont(font);
			g2d.setColor(new Color(133, 13, 13));
			g2d.drawString("min 1, max 5 chars", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("min 1, max 5 chars"),  main.jf.getContentPane().getHeight() - 40);
			g2d.setColor(new Color(78, 78, 78));
			g2d.drawString("cancel - (esc)ape", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("cancel - (esc)ape"),  main.jf.getContentPane().getHeight() - 20);
			g2d.drawString("save - (enter)", main.jf.getContentPane().getWidth() - 10 - getFontMetrics(font).stringWidth("save - (enter)"),  main.jf.getContentPane().getHeight() - 10);
			
			if ( input.done ){
				scorelist.get().add(new Score(input.getText(),game.getScore()));
				scorelist.save();
				queries.removeQuery("NAME");
				game = null;
				menu.openMenu();
			}
		}else{
			g2d.drawImage(new ImageIcon(getClass().getResource("/heart.png")).getImage(), main.jf.getContentPane().getWidth() - 25, 2, this);
			g2d.drawString(Integer.toString( game.getSpaceShip().getLife()), main.jf.getContentPane().getWidth() - 40, 20);
			g2d.drawString(Integer.toString(game.getScore()), 10, 20);
			g2d.setFont(new Font("TimesRoman", Font.PLAIN, 9));
			g2d.setColor(new Color(78, 78, 78));
			g2d.drawString("(p)ause", main.jf.getContentPane().getWidth() - 40,  main.jf.getContentPane().getHeight() - 10);
		}
		
		if ( game != null){
			g2d.drawImage(game.getSpaceShip().getImage(), Math.round(game.getSpaceShip().getX()), Math.round(game.getSpaceShip().getY()), this);
			game.getSpaceShip().getShots().stream().forEach((shot) -> {
				g2d.drawImage(shot.getImage(), Math.round(shot.getX()), Math.round(shot.getY()),this);
			});	

			game.getAliens().getItems().forEach((alien) -> {
				g2d.drawImage(alien.getImage(), Math.round(alien.getX()), Math.round(alien.getY()), this);
			});
			game.getAliens().getShots().forEach((shot)->{
				g2d.drawImage(shot.getImage(), Math.round(shot.getX()), Math.round(shot.getY()),this);
			});
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (game!= null && (game.getSpaceShip().getLife() != 0 || (game.getAliens().getLastAlien().getY() + game.getAliens().getLastAlien().getHeight()) >= game.getSpaceShip().getY())){
			game.getSpaceShip().move();

			if (timer_ten  >= ThreadLocalRandom.current().nextInt( game.getSpeed() ,game.getSpeed() + 10 )){
				game.getAliens().getItems().get(ThreadLocalRandom.current().nextInt(0, game.getAliens().getItems().size())).addShot();
			}
			
			if (timer_ten >= game.getSpeed()){
				if (!game.isMoveDown()){ 
					game.getAliens().getItems().forEach( (alien) -> {
						if ( !game.isMoveDown() ){
							if (alien.getX() + alien.getWidth() + 15 >= main.jf.getContentPane().getWidth()){
								game.setMoveLeft(true);
								game.setMoveRight(false);
								game.setMoveDown(true);
							}
							if (alien.getX() + 15 - 25 <= 0){
								game.setMoveRight(true);
								game.setMoveLeft(false);
								game.setMoveDown(true);
							}
						}
					});
				}else{
					game.speedUp();
					game.setMoveDown(false);
				}

				for(Iterator<Alien> aliens = game.getAliens().getItems().listIterator(); aliens.hasNext();){
					Alien alien = aliens.next();

					if (game.isMoveDown()){
						alien.moveDown();
					}
					if ( game.isMoveLeft() && !game.isMoveDown()){
						alien.moveLeft();
					}
					if ( game.isMoveRight() && !game.isMoveDown()){
						alien.moveRight();
					}
				}

				timer_ten = 0;
			}

			game.getSpaceShip().getShots().stream().forEach( (shot) ->{
				shot.forwardShot();
			});

			game.getAliens().getItems().forEach( (alien) -> {
				alien.getShots().forEach( (shot) ->{
					shot.forwardShot();
				});
			});

			for(Iterator<Alien> aliens = game.getAliens().getItems().listIterator(); aliens.hasNext();){
				Alien alien = aliens.next();

				for(Iterator<Shot> shots = alien.getShots().listIterator(); shots.hasNext();){
					Shot shot = shots.next();

					if ( ((shot.getX() >= game.getSpaceShip().getX()) && (shot.getX() <= game.getSpaceShip().getX() + game.getSpaceShip().getWidth())) && ((shot.getY() <= game.getSpaceShip().getY() + game.getSpaceShip().getHeight()) && (shot.getY() >= game.getSpaceShip().getY())) ) {
						shots.remove();
						game.getSpaceShip().removeLife();
					}else{
						if ( shot.getY() > main.jf.getContentPane().getHeight() ){
							shots.remove();
						}
					}
				}

				for(Iterator<Shot> shots = game.getSpaceShip().getShots().listIterator(); shots.hasNext();){
					Shot shot = shots.next();
					if ( ((shot.getX() >= alien.getX()) && (shot.getX() <= alien.getX() + alien.getWidth())) && ((shot.getY() <= alien.getY() + alien.getHeight()) && (shot.getY() >= alien.getY())) ) {
						shots.remove();
						aliens.remove();
						game.addScore(10);
					}else{
						if ( shot.getY() < 0 ){
							shots.remove();
						}
					}
				}
			}

			if (game.getAliens().getItems().size() == 0){
				game.nextLevel();
			}
		}
		
		repaint();
	}

	private class TAdapter extends KeyAdapter {

		@Override
		public void keyReleased(KeyEvent e){
			
			if ( queries.hasQueries()){
				queries.keyReleased(e);
			}else{
				if (e.getKeyCode() == KeyEvent.VK_P && game != null){
					menu.openCloseMenu();
				}
				if (menu.isOpen()){
					menu.keyReleased(e);
				}else{
					game.getSpaceShip().keyReleased(e);
				}
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (! menu.isOpen()){
				game.getSpaceShip().keyPressed(e);
			}
		}
	}
}