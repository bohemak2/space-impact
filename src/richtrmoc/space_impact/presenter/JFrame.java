/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.presenter;

import javax.swing.plaf.DimensionUIResource;

/**
 *
 * @author lukas.richtrmoc
 */
public class JFrame extends javax.swing.JFrame {
	public static Board board;
	
	public JFrame (){
		setResizable(false);
		setPreferredSize(new DimensionUIResource(800, 600));
		pack();
		setTitle("SPACE IMPACT");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public void addBoard(){
		board = new Board();
		add(board);
	}
}
