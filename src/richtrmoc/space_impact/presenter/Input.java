/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.presenter;

import java.awt.event.KeyEvent;
import richtrmoc.space_impact.interfaces.Query;

/**
 *
 * @author lukas.richtrmoc
 */
public class Input extends BaseQuery implements Query{
		String text;
		int max;
		int min;
		
		public Input() {
			this.text = "";
			this.max = Integer.MAX_VALUE;
			this.min = 0;
		}

		public void setText(String text) {
			this.text = text;
		}
		
		public void setMax(int max) {
			this.max = max;
		}

		public void setMin(int min) {
			this.min = min;
		}

		public int getMax() {
			return max;
		}

		
		@Override
		public void keyReleased(KeyEvent e){
			int key = e.getKeyCode();

			if(key == KeyEvent.VK_ESCAPE){
				setResult(false);
				setDone(true);
			}

			if (key == KeyEvent.VK_ENTER && this.min <= text.length()){
				setResult(true);
				setDone(true);
			}

			if (key == KeyEvent.VK_BACK_SPACE){
				if (text.length() > 0){
					text = text.substring(0, text.length() - 1);
				}else{
					text = "";
				}
			}else{
				if( Character.isUnicodeIdentifierPart(e.getKeyChar()) && text.length() < this.max ){
					text += e.getKeyChar();
				}
			}
		}

		public String getText() {
			return text;
		}
	}