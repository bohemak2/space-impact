/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.presenter;

import java.awt.Font;
import java.awt.event.KeyEvent;
import richtrmoc.space_impact.main;
import richtrmoc.space_impact.model.Game;

/**
 *
 * @author lukas.richtrmoc
 */
public class Menu{
		
		private String[] items = {"NEW GAME","SCORE","EXIT"};
		
		private int pointer;
		
		private boolean open;
		
		public Menu() {
			pointer = 0;
			open = true;
			
			openMenu();
		}

		
		public void openMenu(){
			if (open == false){
				open = true;
			}
		}
		
		public void closeMenu(){
			if (open != false){
				open = false;
			}
		}
		
		public void openCloseMenu(){
			if ( open == true){
				closeMenu();
			}else{
				openMenu();
			}
		}

		public int getPointer() {
			return pointer;
		}
		
		public void setPointer(int pointer) {
			if ( this.pointer != pointer)
				this.pointer = pointer;
		}

		public String[] getItems() {
			return items;
		}
		
		public void selectItem(){
			switch(pointer){
				case 0: 
					JFrame.board.setGame(new Game());
					closeMenu();
					break;
				case 1:
					JFrame.board.getQueries().addQuery("SCORE", new Score());
					JFrame.board.changeScorelist(true);
					break;
				case 2:
					main.jf.dispose();
					System.exit(0);
					break;
			}
		}

		public boolean isOpen() {
			return open;
		}
		
		public int getCenter(Font font){
			return ((main.jf.getContentPane().getWidth() - main.jf.getFontMetrics(font).stringWidth("NEW GAME")) / 2 );
		}
		
		public int getMiddle(Font font){
			return ((main.jf.getContentPane().getHeight()- (items.length * font.getSize())) /2);
		}

		public void keyReleased(KeyEvent e) {
			int key = e.getKeyCode();

			if (key == KeyEvent.VK_UP) {
				if ( pointer != 0){
					pointer--;
				}else{
					pointer = items.length -1;
				}
			}

			if (key == KeyEvent.VK_DOWN) {
				if ( pointer != items.length -1){
					pointer++;
				}else{
					pointer = 0;
				}
			}

			if (key == KeyEvent.VK_ENTER){
				selectItem();
			}
		}
	}
