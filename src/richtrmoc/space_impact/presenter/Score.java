/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.presenter;

import java.awt.event.KeyEvent;
import richtrmoc.space_impact.interfaces.Query;

/**
 *
 * @author lukas.richtrmoc
 */
public class Score extends BaseQuery implements Query{

	private int pointer = 0;
	
	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_ESCAPE){
			result = false;
			setDone(true);
		}
		if (key == KeyEvent.VK_UP) {
			pointer--;
		}

		if (key == KeyEvent.VK_DOWN) {
			pointer++;
		}
		
		if(key == KeyEvent.VK_S){
			JFrame.board.searchScorelist();
		}
		
		if(key == KeyEvent.VK_E){
			JFrame.board.editScorelist(pointer);
		}

		if(key == KeyEvent.VK_T){
			JFrame.board.changeScorelist();
		}

		if(key == KeyEvent.VK_DELETE){
			JFrame.board.clearScorelist(pointer);
		}
	}

	public int getPointer() {
		return pointer;
	}

	public void setPointer(int pointer) {
		this.pointer = pointer;
	}
	
}
