/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

import java.util.ArrayList;

/**
 *
 * @author lukas.richtrmoc
 */
public class Aliens{
	private ArrayList<Alien> data;

	public Aliens() {
		data = new ArrayList<Alien>();
	}
	
	public void addAlien(Alien alien){
		data.add(alien);
	}
	
	public void removeAlien(Alien alien){
		data.remove(alien);
	}
	
	public ArrayList<Shot> getShots(){
		ArrayList<Shot> shots = new ArrayList<Shot>();
		for(Alien alien : data)
		{
			for (Shot shot : alien.getShots()){
				shots.add(shot);
			}
		}
		return shots;
	}

	public ArrayList<Alien> getItems() {
		return data;
	}
	
	public Alien getLastAlien(){
		return data.get(data.size() - 1);
	}
	
}
