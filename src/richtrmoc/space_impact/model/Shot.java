/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author lukas.richtrmoc
 */
public class Shot{
	
	private float x;
	
	private float y;
	
	private Shooter shooter;
	
	private Image img;

	public Shot(Shooter shooter) {
		x = shooter.getX() + (shooter.getImage().getWidth(null)/2);
		y = shooter.getY() + (shooter.getImage().getHeight(null)/2);
		this.shooter = shooter;
		if ( shooter instanceof SpaceShip){
			img = new ImageIcon(getClass().getResource("/missile.png")).getImage();
		}else{
			img = new ImageIcon(getClass().getResource("/missile_alien.png")).getImage();
		}
		
	}

	public Image getImage() {
		return img;
	}
	
	public float getY() {
		return y;
	}
	
	public void forwardShot(){
		if ( shooter instanceof SpaceShip){
			y -= 7;
		}else{
			y += 5;
		}
	}

	public void setY(int y) {
		this.y = y;
	}

	public float getX() {
		return x;
	}
}
