/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

/**
 *
 * @author lukas.richtrmoc
 */
public class Level {
	
	private int number;
	
	private int enemies;

	public Level(int number) {
		this.number = number;
		
		this.enemies = number * 7;
	}

	public int getEnemies() {
		return enemies;
	}

	public int getNumber() {
		return number;
	}
}
