/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

import java.awt.Image;
import javax.swing.ImageIcon;


/**
 *
 * @author lukas.richtrmoc
 */
public class Alien extends Shooter{

	private Image[] images;
	
	int im_i;
	
	public Alien(float x,float y) {
		super();
		
		images = new Image[2];
		images[0] = new ImageIcon(getClass().getResource("/alien.png")).getImage();
		images[1] = new ImageIcon(getClass().getResource("/alien_2.png")).getImage();
		
		image =  images[0];
		im_i = 0;
		
		width = 35;
		height = 35;
		
		this.x = x * (this.width + 25);
		this.y = y * (this.height + 10);
	}
	
	public void moveRight(){
		x += 15;
		changeImage();
	}
	
	public void moveLeft(){
		x -= 15;
		changeImage();
	}
	
	public void moveDown(){
		y += height;
	}
	
	private void changeImage(){
		im_i = (im_i == 0) ? 1 : 0;
		image =images[im_i];
	}
}
