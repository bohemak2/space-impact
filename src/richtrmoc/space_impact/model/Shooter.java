/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author lukas.richtrmoc
 */
public class Shooter{
	
	protected float x;
	protected float y;
	protected int width;
	protected int height;
	protected ArrayList <Shot>shots;
	protected Image image;
	
	public Shooter(){
		shots = new ArrayList<Shot>();
	}
	
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public void addShot(){
		shots.add(new Shot(this));
	}
	
	public Image getImage() {
		return image;
	}
	
	public ArrayList<Shot> getShots(){
		return shots;
	}
}
