/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

/**
 *
 * @author lukas.richtrmoc
 */
public class Game {
	
	private int score;
	
	private int level;
	
	private int speed;
	
	private SpaceShip spaceship;
	
	private Aliens aliens;
	
	private boolean moveLeft;
	
	private boolean moveRight;
	
	private boolean moveDown;
	
	public Game(){
		score = 0;
		level = 0;
		nextLevel();
	}
	
	public void nextLevel(){
		level++;
		spaceship = new SpaceShip();
		aliens = new Aliens();
		moveLeft = false;
		moveRight = true;
		moveDown = true;
		setAliens();
		setSpeed();
	}

	public int getScore() {
		return score;
	}
	
	public void addScore(int add){
		score += add;
	}
	
	public SpaceShip getSpaceShip(){
		return spaceship;
	}

	private void setAliens() {
		float iY = 0;
		float iX = 0;
		for ( int i = 1; i  <= level * 3 ; i++){
			if ( (i % 10) == 0){
				iX = 0;
				iY += 1 + ((float)level / 10);
			}
			aliens.addAlien(new Alien(++iX,(float)iY + ((float)level / 5)));
		}
	}

	public boolean isMoveLeft() {
		return moveLeft;
	}

	public void setMoveLeft(boolean moveLeft) {
		this.moveLeft = moveLeft;
	}

	public boolean isMoveRight() {
		return moveRight;
	}

	public void setMoveRight(boolean moveRight) {
		this.moveRight = moveRight;
	}

	public boolean isMoveDown() {
		return moveDown;
	}

	public void setMoveDown(boolean moveDown) {
		this.moveDown = moveDown;
	}

	public int getSpeed() {
		return speed + (aliens.getItems().size() * 5);
	}
	
	public void speedUp(){
		if (speed > 100){ 
			speed -= 35;
		}
	}

	private void setSpeed() {
		speed = 300 - (level * 10) + (aliens.getItems().size() * 5);
	}

	public int getLevel() {
		return level;
	}
	
	public Aliens getAliens() {
		return aliens;
	}
}
