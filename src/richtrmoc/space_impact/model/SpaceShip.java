/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import richtrmoc.space_impact.main;

/**
 *
 * @author lukas.richtrmoc
 */
public class SpaceShip extends Shooter{

	private int dx;
	
	private int life;
	
	public SpaceShip() {
		super();
		
		life = 3;
		image = new ImageIcon(getClass().getResource("/ship.png")).getImage();
		width = image.getWidth(null);
		height = image.getHeight(null);
		x = 200;
		y = main.jf.getContentPane().getHeight() - image.getHeight(null);
	}

	public void move() {
		if ( 0 <= this.getX() && dx < 0){
			x += dx;
		}else{
			if ( main.jf.getContentPane().getWidth() - this.image.getWidth(main.jf) >= this.getX() && dx > 0 ){
				x += dx;
			}
		}
	}

	public void keyPressed(KeyEvent e) {

		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			dx = -3;
		}

		if (key == KeyEvent.VK_RIGHT) {
			dx = 3;
		}
	}

	public void keyReleased(KeyEvent e) {

		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			dx = 0;
		}

		if (key == KeyEvent.VK_RIGHT) {
			dx = 0;
		}
		
		if (key == KeyEvent.VK_SPACE){
			addShot();
		}
	}
	
	public int getLife() {
		return life;
	}

	public void removeLife() {
		life--;
	}
}