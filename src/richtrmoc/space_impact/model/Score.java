/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

/**
 *
 * @author lukas.richtrmoc
 */
public class Score {
	private String name;
	
	private int result;
	
	private boolean show;

	public Score(String name, int result) {
		this.name = name;
		this.result = result;
		show = true;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getName() {
		return name;
	}

	public int getResult() {
		return result;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}
}
