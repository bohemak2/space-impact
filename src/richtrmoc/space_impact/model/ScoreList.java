/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lukas.richtrmoc
 */
public class ScoreList {
	
	private ArrayList<Score> data;
	
	private File home;
	
	private boolean sortBest;

	public ScoreList() {
		data = new ArrayList<>();
		home = new File ( System.getProperty("user.home") + "/Documents/Space Impact/");
		sortBest = true;
		if ( ! home.exists()){
			home.mkdirs();
		}
		home = new File( home.getPath() + "/scorelist.csv");
		if( !home.exists() ){
			try {
				home.createNewFile();
			} catch (IOException ex) {
				Logger.getLogger(ScoreList.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		load();
	}
	
	public ArrayList<Score> get(){
		return data;
	}
	
	public void load(){
		try {
			BufferedReader vstup = new BufferedReader(new InputStreamReader(new FileInputStream(home)));
			String radek;
			while ((radek = vstup.readLine())!=null) {
				StringTokenizer st = new StringTokenizer(radek,";");
				data.add(new Score((String)st.nextToken(), Integer.parseInt(st.nextToken())));
			}
			sort();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public void save(){
		try{
			OutputStreamWriter vystup = new OutputStreamWriter(new FileOutputStream(home));
			
			data.forEach((k) -> {
				try {
					vystup.write(k.getName() + ";" + k.getResult() + "\n");
				} catch (IOException ex) {
					Logger.getLogger(ScoreList.class.getName()).log(Level.SEVERE, null, ex);
				}
			});
			
			vystup.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void sort(){
		if ( sortBest ){
			data.sort((o1,o2) -> o2.getResult() - o1.getResult());
			sortBest = false;
		}else{
			data.sort((o1,o2) -> o1.getResult() - o2.getResult());
			sortBest = true;
		}
	}
	
	public void sort (boolean best){
		if ( best ){
			data.sort((o1,o2) -> o2.getResult() - o1.getResult());
		}else{
			data.sort((o1,o2) -> o1.getResult() - o2.getResult());
		}
	}
	
	
	public void search(String text){
		data.forEach((score) -> {
			if ( !score.getName().contains(text)){
				score.setShow(false);
			}else{
				score.setShow(true);
			}
		});
	}
}