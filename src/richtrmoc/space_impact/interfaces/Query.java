/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtrmoc.space_impact.interfaces;

import java.awt.event.KeyEvent;

/**
 *
 * @author lukas.richtrmoc
 */
public interface Query {
	public void keyReleased(KeyEvent e);
	
	public boolean isDone();
	
	public boolean isResult();
	
	public boolean isPause();
}
